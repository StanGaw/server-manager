import React from "react";
import { RecoilRoot } from "recoil";
import ServerList from "./components/serverList/ServerList";
import Layout from "./components/layout/Layout";
import Header from "./components/header/Header";
import "./App.css";
const App = () => {
  return (
    <RecoilRoot>
      <Header />
      <Layout>
        <ServerList />
      </Layout>
    </RecoilRoot>
  );
};

export default App;
