import { atom } from "recoil";
export const serverAtom = atom({
  key: "server",
  default: [],
});
