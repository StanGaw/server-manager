import React from "react";
import Menu from "../menu/Menu";
import Close from "../icons/Close";
import { ONLINE, OFFLINE, REBOOTING } from "../types/types";
import styles from "./styles.module.css";

const ServerListItem = ({ children, status, id, isOpen }) => {
  const statusStlying = () => {
    if (status === ONLINE)
      return (
        <>
          <span className={`${styles.statusIcon} ${styles.circle}`}></span>{" "}
          <p>{status}</p>
        </>
      );
    if (status === OFFLINE)
      return (
        <>
          <span className={`${styles.statusIcon} ${styles.close}`}>
            <Close />
          </span>{" "}
          <p>{status}</p>
        </>
      );
    if (status === REBOOTING)
      return (
        <>
          <span className={`${styles.statusIcon} ${styles.dot}`}></span>{" "}
          <p>{status}...</p>
        </>
      );
  };
  return (
    <li className={styles.server}>
      <div className={styles.server__name}>{children}</div>
      <div
        className={`${styles.server__status} ${
          styles[`${status.toLowerCase()}_text`]
        }`}
      >
        {statusStlying()}
      </div>
      <div className={styles.server__menu}>
        <Menu isOpen={isOpen} id={id} status={status} />
      </div>
    </li>
  );
};

export default ServerListItem;
