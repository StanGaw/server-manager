export const menuHandler = ([id, servers, setServers]) => {
  const newState = [...servers];
  newState.forEach((server, serverOrder) => {
    const closeMenu = {
      ...server,
      isOpen: false,
    };
    if (id === server.id) {
      const openCloseMenu = {
        ...server,
        isOpen: !server.isOpen,
      };
      newState.splice(serverOrder, 1, openCloseMenu);
    } else {
      newState.splice(serverOrder, 1, closeMenu);
    }
  });

  setServers(newState);
};
