import React from "react";
import { serverAtom } from "../../atoms/serverAtom";
import { useRecoilState } from "recoil";
import { ONLINE, OFFLINE, REBOOTING } from "../types/types";
import { manageServer } from "../../functions/api";
import { menuHandler } from "./menuHelpers";

import Dot from "../icons/Dot";
import styles from "./styles.module.css";
const Menu = ({ id, isOpen, status }) => {
  const [servers, setServers] = useRecoilState(serverAtom);
  const menuHandlerEntries = [id, servers, setServers];

  const renderMenu = () => {
    let menu = [];
    (() => {
      if (status === ONLINE) return (menu = ["Turn Off", "Reboot"]);
      if (status === OFFLINE) return (menu = ["Turn On"]);
      if (status === REBOOTING) return (menu = null);
    })();

    menu = (
      <div className={styles.option}>
        {menu &&
          menu.map((item) => {
            return (
              <div
                key={item}
                onClick={() => manageServer(id, item, setServers, servers)}
                className={styles.option__item}
              >
                {item}
              </div>
            );
          })}
        <div
          onClick={() => {
            menuHandler(menuHandlerEntries);
          }}
          className={styles.outside}
        ></div>
      </div>
    );

    return menu;
  };
  return (
    <>
      <div
        onClick={() => menuHandler(menuHandlerEntries)}
        className={styles.menu}
      >
        <div className={styles.dot}>
          <Dot />
        </div>
      </div>
      {isOpen && renderMenu()}
    </>
  );
};

export default Menu;
