import React, { useEffect } from "react";
import { useRecoilState } from "recoil";
import { serverAtom } from "../../atoms/serverAtom";
import ServerListItem from "../serverListItem/ServerListItem";
import Counter from "../counter/Counter";
import { fetchServers } from "../../functions/api";
import styles from "./styles.module.css";

const ServerList = () => {
  const [servers, setServers] = useRecoilState(serverAtom);
  useEffect(() => {
    fetchServers().then((data) => {
      const newData = data.map((el) => ({
        ...el,
        isOpen: false,
      }));
      setServers(newData);
    });
  }, []);

  const heading = (
    <li className={styles.heading}>
      <div className={styles.heading__name}>Name</div>
      <div className={styles.heading__status}>Status</div>
      <div className={styles.heading__placeholder}></div>
    </li>
  );
  const listing = (
    <ul className={styles.listWrapper}>
      {heading}
      {servers &&
        servers.length > 0 &&
        servers.map((el) => {
          return (
            <ServerListItem
              id={el.id}
              key={el.name}
              status={el.status}
              isOpen={el.isOpen}
            >
              {el.name}
            </ServerListItem>
          );
        })}
    </ul>
  );
  return (
    <>
      <Counter count={servers && servers.length} />
      {listing}
    </>
  );
};

export default ServerList;
