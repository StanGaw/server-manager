import React from "react";
import styles from "./styles.module.css";
const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.cricle}></div>
      <p className={styles.headingText}>Recruitment Task</p>
    </div>
  );
};

export default Header;
