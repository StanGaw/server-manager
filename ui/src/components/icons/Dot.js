import React from "react";

function Dot() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      data-name="24x24/On Light/Dots"
      viewBox="0 0 24 24"
    >
      <path
        fill="var(--lightBlue)"
        d="M12 1.5A1.5 1.5 0 1113.5 3 1.5 1.5 0 0112 1.5zm-6 0A1.5 1.5 0 117.5 3 1.5 1.5 0 016 1.5zm-6 0A1.5 1.5 0 111.5 3 1.5 1.5 0 010 1.5z"
        transform="translate(4.5 11)"
      ></path>
    </svg>
  );
}

export default Dot;
