import React from "react";
import styles from "./styles.module.css";
const Counter = ({ count }) => {
  return (
    <div className={styles.counter}>
      <h2 className={styles.heading}>Servers</h2>
      <p className={styles.elements_amount}>Number of elements : {count}</p>
    </div>
  );
};

export default Counter;
