import { ONLINE } from "../components/types/types";

export async function fetchServers() {
  const url = "http://localhost:4454/servers";
  const rsp = await fetch(url);
  const result = rsp.json();
  return result;
}

export async function fetchServer(id) {
  const url = `http://localhost:4454/servers/${id}`;
  const response = await fetch(url);
  const result = response.json();
  return result;
}
export async function fetchPutCurrentServer(id, state) {
  const url = `http://localhost:4454/servers/${id}/${state}`;
  const response = await fetch(url, { method: "PUT" });
  const result = response.json();
  return result;
}
export const manageServer = (id, item, setServers, servers) => {
  if (item === "Turn On") {
    fetchPutCurrentServer(id, "on").then((data) => {
      setServerState(servers, setServers, data);
    });
  } else if (item === "Turn Off") {
    fetchPutCurrentServer(id, "off").then((data) => {
      setServerState(servers, setServers, data);
    });
  } else {
    fetchPutCurrentServer(id, "reboot").then((data) => {
      if (data.status !== ONLINE) {
        const intervalId = setInterval(() => {
          fetchServer(id).then((serverData) => {
            setServerState(servers, setServers, serverData);
            if (serverData.status === ONLINE) return clearInterval(intervalId);
          });
        }, 1000);
      }
    });
  }
};
export const setServerState = (servers, setServers, data) => {
  const newState = [...servers];
  newState.forEach((server, serverOrder) => {
    if (data.id === server.id) {
      const updatedServer = {
        ...server,
        isOpen: false,
        status: data.status,
      };
      newState.splice(serverOrder, 1, updatedServer);
    }
  });
  setServers(newState);
};
